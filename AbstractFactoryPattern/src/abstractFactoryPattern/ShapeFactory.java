package abstractFactoryPattern;

public class ShapeFactory extends AbstractFactory{
	
	@Override
	Shape getShape(String shapeType) {
		if(shapeType == null)
		{
			return null;
		}
		
		if(shapeType.equalsIgnoreCase("CIRCLE"))
		{
			return new Circle();
		}
		else
		if(shapeType.equalsIgnoreCase("rectangle"))
		{
			return new Rectangle();
		}
		else
		if(shapeType.equalsIgnoreCase("Square")){
			return new Square();		
		}
		
		return null;
	}
	
	@Override
	Color getColor(String colorType) {
		return null;
	}
}
