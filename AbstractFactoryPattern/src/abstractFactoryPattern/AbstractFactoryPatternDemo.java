package abstractFactoryPattern;

public class AbstractFactoryPatternDemo {
	
	public static void main(String[] args)
	{
		//get shape factory
		AbstractFactory shapeFactory = FactoryProducer.getFactory("SHAPE");
		
		//get an object of shape circle
		Shape shape1 = shapeFactory.getShape("Circle");
		
		//call draw method of shape circle
		shape1.draw();
		
		//get an object of shape rectangle
		Shape shape2 = shapeFactory.getShape("rectangle");
		
		//call draw method of SHape Rectangle
		shape2.draw();
		
		//get an object of Shape Square
		Shape shape3 = shapeFactory.getShape("Square");
		
		//call draw method of shape square
		shape3.draw();
		
		//get color factory
		AbstractFactory colorFactory = FactoryProducer.getFactory("Color");
		
		// get an objec of Color Red
		Color color1 = colorFactory.getColor("RED");
		
		// call fill method of RED
		color1.fill();
		
		// get an object of color Green
		Color color2 = colorFactory.getColor("green");
		
		// call fill method of green
		color2.fill();
		
		// get an object of color blue
		Color color3 = colorFactory.getColor("blue");
		
		// call fill method of color blue
		color3.fill();
	}

}
