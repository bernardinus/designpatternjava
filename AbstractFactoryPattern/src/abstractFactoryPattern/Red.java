package abstractFactoryPattern;

public class Red implements Color {

	@Override
	public void fill() {
		System.out.println("Invasi Red::fill() method.");

	}

}
