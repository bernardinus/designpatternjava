package singletonPattern;

public class SingletonPatternDemo {
	public static void main(String[] args)
	{
		//illegal construct
		//Compile Time Error : the constructor SingleObject() is not visible
		//SingleObject obj = new SingleObject();
		
		// get the only object available
		SingleObject object = SingleObject.getInstance();
		
		//show the message
		object.showMessage();
	}
}
