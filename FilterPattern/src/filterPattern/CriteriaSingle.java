package filterPattern;

import java.util.List;
import java.util.ArrayList;

public class CriteriaSingle implements Criteria {

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> singlePersons = new ArrayList<Person>();
		for(Person person : persons)
		{
			if(person.getMaritalStatus().equalsIgnoreCase("single"))
			{
				singlePersons.add(person);
			}
		}
		return singlePersons;
	}

}
